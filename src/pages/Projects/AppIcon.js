import React from 'react';
import { Row, Col } from 'antd';
import { Link } from 'react-router-dom';

function AppIcon({
  name, to, year, img, size = 100, style,
}) {
  return (
    <div style={styles.main(style)}>
      <Row justify="center">
        <Col>
          {to && to[0] === '/'
            ? (
              <Link to={to}>
                <img
                  src={img ? `/images/${img}` : '/images/profile_pic_simp.png'}
                  alt="project"
                  height={size}
                  width={size}
                  style={styles.icon(false)}
                />
              </Link>
            )
            : (
              <img
                src={img ? `/images/${img}` : '/images/profile_pic_simp.png'}
                alt="project"
                height={size}
                width={size}
                style={styles.icon(to)}
                onClick={() => {
                  if (to && to[0] !== '/') {
                    window.location.href = to;
                  }
                }}
              />
            )}
        </Col>
      </Row>
      <Row justify="center">
        <Col>
          {name && (
          <p style={styles.name(size)}>
            {name}
            ,
            {' '}
            {year}
          </p>
          )}
        </Col>
      </Row>
    </div>
  );
}

const styles = {
  main: (style) => ({
    margin: 10,
    ...style,
  }),
  icon: (showPointer) => ({
    borderRadius: 20,
    ...(showPointer && { cursor: 'pointer' }),
  }),
  name: (size) => ({
    maxWidth: size,
  }),
};

export default AppIcon;
