import React, { useCallback } from 'react';
import { Row } from 'antd';
import AppIcon from './AppIcon';
import history from '../../history';

function BottomBar({ setBarHeight, inApp = false }) {
  const getBarHeight = useCallback((node) => {
    if (node !== null) {
      setBarHeight(node.getBoundingClientRect().height);
    }
  }, []);

  return (
    <div
      style={styles.main(inApp)}
      ref={getBarHeight}
    >
      {inApp
        ? (
          <Row justify="center">
            <img
              onClick={() => history.back()}
              src="/images/ioshome.png"
              alt="home button"
              height={90}
              width={90}
              style={styles.homeButton}
            />
          </Row>
        )
        : (
          <Row justify="center">
            <AppIcon to="mailto:elias@lankinen.xyz" img="ios1.png" size={90} />
            <AppIcon to="/about/media" img="ios2.png" size={90} style={styles.favoriteButtons} />
            <AppIcon to="https://www.notion.so/lankinen/Lankinen-92c1d0531acd48f99c4dcdd546ad1a68" img="ios3.png" size={90} />
          </Row>
        )}
    </div>
  );
}

const styles = {
  main: (inApp) => ({
    backgroundColor: inApp ? 'black' : 'lightgray',
    width: '100%',
    position: 'sticky',
    bottom: 0,
  }),
  homeButton: {
    margin: 10,
    cursor: 'pointer',
  },
  favoriteButtons: {
    paddingLeft: '3%',
    paddingRight: '3%',
  },
};

export default BottomBar;
