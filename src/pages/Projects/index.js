import React, { useEffect, useState } from 'react';
import {
  useParams,
} from 'react-router-dom';
import { Row } from 'antd';
import Grid from '../../components/Grid';
import projectsData from '../../data/projects.json';
import AppIcon from './AppIcon';
import ProjectInfo from './ProjectInfo';
import useWindowSize from '../../hooks/useWindowSize';
import BottomBar from './BottomBar';

function Projects() {
  const windowSize = useWindowSize();
  const params = useParams();
  const { id } = params;

  const [barHeight, setBarHeight] = useState(0);
  const [waitTime, setWaitTime] = useState(id === undefined);

  useEffect(() => {
    if (waitTime) {
      setTimeout(() => {
        setWaitTime(false);
      }, 1000);
    }
  }, [waitTime]);

  if (windowSize.width === undefined) return null;
  if (waitTime) {
    return (
      <Row
        style={{
          backgroundColor: 'black', width: '100%', height: windowSize.height - 64, alignItems: 'center',
        }}
        justify="center"
      >
        <img src="/images/apple.png" style={{ height: 100 }} alt="apple" />
      </Row>
    );
  }

  const selectedProject = projectsData.filter((item) => item.name === id)[0];

  let perRow = 6;
  if (windowSize.width < 500) {
    perRow = 2;
  } else if (windowSize.width < 800) {
    perRow = 4;
  }

  return (
    <>
      {(!id || selectedProject === undefined)
        ? (
          <div style={styles.homeMain}>
            <div style={styles.homeContainer(windowSize.height - barHeight - 64)}>
              <Grid
                data={projectsData}
                perRow={perRow}
                itemComponent={(item) => (
                  <AppIcon name={item.name} to={`/projects/${item.name}`} year={item.year} img={item.icon} />
                )}
              />
            </div>
            <BottomBar setBarHeight={setBarHeight} />
          </div>
        )
        : (
          <div>
            <ProjectInfo project={selectedProject} windowHeight={windowSize.height} height={windowSize.height - barHeight - 64 - 20} setBarHeight={setBarHeight} />
          </div>
        )}
    </>
  );
}

const styles = {
  homeMain: {
    backgroundImage: 'url(/images/mountain.png)',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
  },
  homeContainer: (minHeight) => ({
    minHeight,
  }),
};

export default Projects;
