import React, { useEffect, useState } from 'react';
import { Carousel, Row } from 'antd';
import AppIcon from './AppIcon';
import Container from '../../components/Container';
import BottomBar from './BottomBar';

function ProjectInfo({
  project, windowHeight, height, setBarHeight,
}) {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 500);
  }, []);

  if (loading) {
    return (
      <Row
        style={styles.blackMain(windowHeight)}
        justify="center"
      >
        <AppIcon
          img={project.icon}
        />
      </Row>
    );
  }

  return (
    <>
      <Container justify="left" style={styles.inAppMain(height)}>
        <h1>
          {project.name}
          ,
          {' '}
          {project.year}
        </h1>
        <p><span style={styles.bold}>Idea:</span> {project.idea}</p>
        <div>
          {project.images.map(url => (
            <img src={url} style={styles.productImage} />
          ))}
        </div>
        <p><span style={styles.bold}>Tech:</span> {project.tech}</p>
        <p><span style={styles.bold}>Reason to quit:</span> {project.quit}</p>
        <p><span style={styles.bold}>What I learned:</span> {project.learnings}</p>
        {project.urls?.map((url) => (
          <a href={url} key={url}>{url}</a>
        ))}
      </Container>
      <BottomBar setBarHeight={setBarHeight} inApp />
    </>
  );
}

const styles = {
  blackMain: (windowHeight) => ({
    height: windowHeight - 64,
    width: '100%',
    backgroundColor: 'white',
    alignItems: 'center',
  }),
  inAppMain: (minHeight) => ({
    minHeight,
  }),
  bold: {
    fontWeight: 1000
  },
  productImage: {
    maxHeight: 500,
    padding: 10
  }
};

export default ProjectInfo;
