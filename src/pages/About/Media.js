import React from 'react';

import media from '../../data/media.json';

function Media() {
  return (
    <ul>
      {media.map((item) => (
        <li key={item.url}>
          <a href={item.url} target="_blank" rel="noreferrer">
            <p style={styles.linkText}>{item.url}</p>
          </a>
        </li>
      ))}
    </ul>
  );
}

const styles = {
  linkText: {
    wordWrap: 'break-word',
  },
};

export default Media;
