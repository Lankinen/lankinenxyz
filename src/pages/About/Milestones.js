import React, { useState } from 'react';
import { Row, Col } from 'antd';
import { Slider } from 'react95';
import milestones from '../../data/milestones.json';

function Milestones() {
  const [selected, setSelected] = useState(0);

  const selectedMilestone = milestones.filter((item) => item.value === selected)[0];
  return (
    <Row>
      <Col xs={7} sm={6} md={4}>
        <Slider
          size="300px"
          min={0}
          max={20}
          step={1}
          value={selected}
          onChange={(e, value) => {
            let roundedValue = -1;
            let lastDelta = 100000;
            milestones.forEach((item) => {
              const delta = Math.abs(item.value - value);
              if (delta <= lastDelta) {
                lastDelta = delta;
                roundedValue = item.value;
              }
            });
            setSelected(roundedValue);
          }}
          marks={milestones}
          orientation="vertical"
        />
      </Col>
      <Col xs={17} sm={18} md={20}>
        <h1>{selectedMilestone.title}</h1>
        <p>
          {selectedMilestone.text}
        </p>
      </Col>
    </Row>
  );
}

export default Milestones;
