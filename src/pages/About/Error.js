import React, { useState, useEffect } from 'react';
import { Progress } from 'react95';

function Error() {
  const [percent, setPercent] = useState(0);

  useEffect(() => {
    const timer = setInterval(() => {
      setPercent((previousPercent) => {
        if (previousPercent === 99) {
          clearInterval(timer);
          return 99;
        }
        const diff = Math.random() * 20;
        return Math.min(previousPercent + diff, 99);
      });
    }, 500);
    return () => {
      clearInterval(timer);
    };
  }, []);

  return <Progress value={Math.floor(percent)} />;
}

export default Error;
