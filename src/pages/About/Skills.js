import React, { useState, useEffect } from 'react';
import { Row, Col } from 'antd';
import { Progress } from 'react95';
import programmingLanguages from '../../data/skills_languages.json';
import tools from '../../data/skills_tools.json';

function Skills() {
  return (
    <>
      <h2>Programming Languages</h2>
      <Row>
        {programmingLanguages.map((item) => (
          <SkillLevelSlider title={item.name} level={item.level} key={item.name} />
        ))}
      </Row>
      <h2>Tools</h2>
      <Row>
        {tools.map((item) => (
          <SkillLevelSlider title={item.name} level={item.level} key={item.name} />
        ))}
      </Row>
    </>
  );
}

const styles = {
  sliderCol: {
    padding: 25,
  },
};

export default Skills;

let to1 = null;

function SkillLevelSlider({ title, level }) {
  const [percent, setPercent] = useState(0);

  useEffect(() => {
    to1 = setTimeout(() => {
      setPercent(level);
    }, 500);
    return () => {
      if (to1) {
        clearTimeout(to1);
      }
    };
  }, []);

  return (
    <Col style={styles.sliderCol}>
      <p>{title}</p>
      <Progress style={{ width: 200 }} value={percent} />
    </Col>
  );
}
