import React, { useState, useEffect } from 'react';
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import {
  useParams,
  useHistory,
} from 'react-router-dom';
import {
  Tabs,
  Tab,
  TabBody,
  Window,
  WindowHeader,
  WindowContent,
  Hourglass,
} from 'react95';
import original from 'react95/dist/themes/original';
import msSansSerif from 'react95/dist/fonts/ms_sans_serif.woff2';
import msSansSerifBold from 'react95/dist/fonts/ms_sans_serif_bold.woff2';
import useWindowSize from '../../hooks/useWindowSize';
import Me from './Me';
import Skills from './Skills';
import Milestones from './Milestones';
import Media from './Media';
import Error from './Error';

const GlobalStyles = createGlobalStyle`
  @font-face {
    font-family: 'ms_sans_serif';
    src: url('${msSansSerif}') format('woff2');
    font-weight: 400;
    font-style: normal
  }
  @font-face {
    font-family: 'ms_sans_serif';
    src: url('${msSansSerifBold}') format('woff2');
    font-weight: bold;
    font-style: normal
  }
  body {
    font-family: 'ms_sans_serif';
  }
`;

const About = () => {
  const history = useHistory();
  const windowSize = useWindowSize();

  const params = useParams();
  const { id } = params;
  const activeTab = id || 'me';

  const [waitTime, setWaitTime] = useState(true);

  useEffect(() => {
    if (waitTime) {
      setTimeout(() => {
        setWaitTime(false);
      }, 1000);
    }
  }, [waitTime]);

  return (
    <div>
      <GlobalStyles />
      <ThemeProvider theme={original}>
        <Window style={{ width: '100%', minHeight: windowSize.height ? windowSize.height - 64 : 0 }}>
          <WindowHeader>
            <span role="img" aria-label="usa">
              🇺🇸
            </span>
            lankinen.exe
          </WindowHeader>
          <WindowContent>
            <Tabs
              value={activeTab}
              onChange={(e, value) => {
                setWaitTime(true);
                history.push(`/about/${value}`);
              }}
            >
              <Tab value="me">
                Me
              </Tab>
              {/* <Tab value="skills">
                Skills
              </Tab> */}
              <Tab value="milestones">
                Milestones
              </Tab>
              {/* <Tab value="media">
                Media
              </Tab> */}
            </Tabs>
            <TabBody style={{ minHeight: 300 }}>
              {waitTime ? (
                <Hourglass size={32} />
              ) : (
                <>
                  {activeTab === 'me' && (
                  <Me />
                  )}
                  {activeTab === 'skills' && (
                  <Skills />
                  )}
                  {activeTab === 'milestones' && (
                  <Milestones />
                  )}
                  {/* {activeTab === 'media' && (
                  <Media />
                  )} */}
                  {['me', 'skills', 'milestones', 'media'].indexOf(activeTab) === -1 && (
                  <Error />
                  )}
                </>
              )}
            </TabBody>
          </WindowContent>
        </Window>
      </ThemeProvider>
    </div>
  );
};

export default About;
