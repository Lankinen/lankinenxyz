import React from 'react';

import advdis from '../../data/advdis.json';

function Me() {
  return (
    <>
      <p>Since a young age, I have enjoyed making things. I'm good with a wide range of technologies and I know how to build and launch technical products.</p>
      <p>It's important to know weaknesses and strengths as it allows to work with right kind of people. Here are my biggest weaknesses and strengths.</p>
      <h2>Weaknesses</h2>
      <ul>
        {advdis.disadvantages.map((item) => (
          <CustomListItem key={item.title} title={item.title} description={item.description} />
        ))}
      </ul>
      <h2>Strengths</h2>
      <ul>
        {advdis.advantages.map((item) => (
          <CustomListItem key={item.title} title={item.title} description={item.description} />
        ))}
      </ul>
    </>
  );
}

function CustomListItem({ title, description }) {
  return (
    <li>
      {title}
      <ul>
        <li>{description}</li>
      </ul>
    </li>
  );
}

export default Me;
