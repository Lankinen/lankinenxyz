import React from 'react';
import Terminal from 'terminal-in-react';
import useWindowSize from '../../hooks/useWindowSize';
import { commands, descriptions } from '../../data/commands';
import './index.css';

const mainMessage = "My email address is elias@lankinen.xyz but you can use this machine to send the email. Type 'help' to see all of the commands.";

function Contact() {
  const windowSize = useWindowSize();

  if (windowSize.width === undefined) return null;

  return (
    <div style={{ backgroundColor: 'green' }}>
      <Terminal
        style={{
          caretColor: 'black', fontWeight: 'bold', fontSize: '1.2em', height: windowSize.height - 64, width: '100%',
        }}
        hideTopBar
        allowTabs={false}
        startState="maximised"
        backgroundColor="black"
        barColor="black"
        commands={commands}
        descriptions={descriptions}
        msg={windowSize.width > 900 ? asciiMessage : mainMessage}
        promptSymbol="bash-3.2$"
      />
    </div>
  );
}

const asciiMessage = ` 
88                           88         88                                        
88                           88         ""                                        
88                           88                                                   
88  ,adPPYYba,  8b,dPPYba,   88   ,d8   88  8b,dPPYba,    ,adPPYba,  8b,dPPYba,   
88  ""     \`Y8  88P'   \`"8a  88 ,a8"    88  88P'   \`"8a  a8P_____88  88P'   \`"8a  
88  ,adPPPPP88  88       88  8888[      88  88       88  8PP"""""""  88       88  
88  88,    ,88  88       88  88\`"Yba,   88  88       88  "8b,   ,aa  88       88  
88  \`"8bbdP"Y8  88       88  88   \`Y8a  88  88       88   \`"Ybbd8"'  88       88  

${mainMessage}`;

export default Contact;
