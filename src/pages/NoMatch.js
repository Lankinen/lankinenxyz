import React from 'react';
import { Row } from 'antd';

function NoMatch() {
  return (
    <Row justify="center">
      <img
        src="/images/404.png"
        alt="404"
        style={styles.image}
      />
    </Row>
  );
}

const styles = {
  image: {
    paddingTop: 10,
    maxWidth: '100%',
  },
};

export default NoMatch;
