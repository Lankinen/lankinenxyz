import React, { useState, useEffect } from 'react';
import { Row, Col, Tooltip } from 'antd';

import { TwitterTweetEmbed } from 'react-twitter-embed';
import HelloWidget from './HelloWidget';
import SocialWidget from './SocialWidget';
import useWindowSize from '../../hooks/useWindowSize';
import { getTwitterFollowers, getPinned } from '../../data/twitter';
import getRepos from '../../data/github';

function Home() {
  const windowSize = useWindowSize();

  const [pinnedTweet, setPinnedTweet] = useState();
  useEffect(() => {
    const f = async () => {
      const t = await getPinned();
      setPinnedTweet(t);
    };
    f();
  }, []);

  const [twitterFollowers, setTwitterFollowers] = useState('');
  useEffect(() => {
    const f = async () => {
      const t = await getTwitterFollowers();
      setTwitterFollowers(t);
    };
    f();
  }, []);

  return (
    <div style={{
      width: '100%',
      minHeight: windowSize.height ? windowSize.height - 64 : 0,
      backgroundImage: 'url(/images/blurry.jpg)',
      backgroundPosition: 'center',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',

    }}
    >
      <div style={{ padding: windowSize.width ? windowSize.width * 0.02 : 0 }}>
        <Row justify="space-between">
          <Col>
            <Tooltip title="This is me" placement="bottom">
              <img
                style={{
                  width: 75, height: 75, borderRadius: 50, backgroundColor: 'white',
                }}
                src="/images/profile_pic_simp.png"
                alt="profile"
              />
            </Tooltip>
          </Col>
          <HelloWidget />
        </Row>
        <Row justify="end">
          {windowSize.width > 900 && (
            pinnedTweet && (
            <TwitterTweetEmbed
              tweetId={pinnedTweet}
            />
            )
          )}
        </Row>
      </div>
      <Row
        justify="center"
        style={{ position: 'absolute', width: '100%', bottom: 10 }}
      >
        <SocialWidget icon="/images/twitter.png" text={`${twitterFollowers} Followers`} rgba="rgba(255,30,0,0.8)" color="rgb(255,150,0)" url="https://twitter.com/reallankinen" />
        <SocialWidget icon="/images/notion.png" text="Since 2019" rgba="rgba(50,50,150,0.9)" color="rgb(140,50,180)" url="https://www.notion.so/lankinen/Lankinen-92c1d0531acd48f99c4dcdd546ad1a68" />
        <SocialWidget icon="/images/gitlab.png" text="Gitlab" rgba="rgba(0,30,255,0.9)" color="rgb(0,150,255)" url="https://gitlab.com/lankinen" />
      </Row>
    </div>
  );
}

export default Home;
