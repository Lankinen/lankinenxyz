import React from 'react';
import { Row } from 'antd';
import WidgetWrapper from './WidgetWrapper';
import useWindowSize from '../../hooks/useWindowSize';

function SocialWidget({
  icon, text, rgba, color, url,
}) {
  const windowSize = useWindowSize();

  const multiplier = windowSize.width ? Math.min(windowSize.width + 150, 900) / 900 : 0;

  return (
    <div style={{ padding: 20 * multiplier }}>
      <WidgetWrapper size="small" rgba={rgba} color={color} style={{ paddingLeft: 8 * multiplier }} onClick={() => window.open(url, '_blank')}>
        <Row style={{ paddingTop: 5 * multiplier }}>
          <img src={icon} style={{ width: 60 * multiplier, height: 60 * multiplier }} alt="icon" />
        </Row>
        <Row style={{ paddingTop: 10 * multiplier }}>
          <p style={{
            margin: 0, fontSize: 16 * multiplier, fontWeight: 'bold', color: 'white',
          }}
          >
            {text}
          </p>
        </Row>
      </WidgetWrapper>
    </div>
  );
}

export default SocialWidget;
