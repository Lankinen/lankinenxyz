import React from 'react';
import { Col } from 'antd';
import useWindowSize from '../../hooks/useWindowSize';

const BIGW = 300;
const BIGH = 250;
const SMALLW = 150;
const SMALLH = 125;

function WidgetWrapper({
  size, rgba, color, onClick = null, style, children,
}) {
  const windowSize = useWindowSize();

  const multiplier = windowSize.width ? Math.min(windowSize.width + 200, 1000) / 1000 : 0;
  let width;
  let height;
  if (size === 'small') {
    width = SMALLW * multiplier;
    height = SMALLH * multiplier;
  } else if (size === 'big') {
    width = BIGW * multiplier;
    height = BIGH * multiplier;
  } else {
    width = BIGW * multiplier;
    height = SMALLH * multiplier;
  }

  return (
    <Col style={styles.container(width, height, rgba, color, style, onClick)} onClick={onClick || (() => null)}>
      {children}
    </Col>
  );
}

const styles = {
  container: (width, height, rgba, color, style, onClick) => ({
    width,
    minHeight: height,
    borderRadius: 10,
    padding: 5,
    background: `linear-gradient(transparent, ${rgba})`,
    backgroundColor: color,
    ...(onClick ? { cursor: 'pointer' } : {}),
    ...style,
  }),
};

export default WidgetWrapper;
