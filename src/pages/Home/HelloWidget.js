import React, { useEffect, useState } from 'react';
import { Row, Col } from 'antd';
import WidgetWrapper from './WidgetWrapper';
import useWindowSize from '../../hooks/useWindowSize';
import GhostWriter from '../../components/GhostWriter';
import randomQuote from '../../data/quotes';
import randomJoke from '../../data/jokes';
import getWeather from '../../data/weather';

function HelloWidget() {
  const windowSize = useWindowSize();

  const getTime = () => new Date().toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });

  const [time, setTime] = useState(getTime());
  useEffect(() => {
    setInterval(() => {
      setTime(getTime());
    }, 10000);
  }, []);

  const [joke, setJoke] = useState();
  useEffect(() => {
    const f = async () => {
      const j = await randomJoke();
      setJoke(j);
    };
    f();
  }, []);

  const [quote, setQuote] = useState('');
  useEffect(() => {
    const f = async () => {
      const q = await randomQuote();
      setQuote(q);
    };
    f();
  }, []);

  const [weather, setWeather] = useState();
  useEffect(() => {
    const f = async () => {
      const q = await getWeather();
      setWeather(q);
    };
    f();
  }, []);

  if (joke === undefined) return null;

  const multiplier = windowSize.width ? Math.min(windowSize.width + 300, 900) / 900 : 0;

  return (
    <WidgetWrapper color="rgba(220,220,220,0.92)" size={windowSize.width < 600 ? 'big' : 'medium'}>
      <div style={{
        height: '100%', display: 'flex', flexDirection: 'column', justifyContent: 'space-between',
      }}
      >
        <Row>
          <Col>
            <GhostWriter message={[['Wanna hear a joke?', 2000], [joke[0], 1500], [joke[1], 1000], ["Hope you like it. Here's a random quote.", 1000], [quote, 100]]} />
          </Col>
        </Row>
        <Row justify="space-between">
          <Col>
            <Row>
              <p style={{ margin: 0, fontSize: 12 * multiplier }}>SF</p>
            </Row>
            <Row>
              <h1 style={{ margin: 0, fontSize: 25 * multiplier }}>{weather}</h1>
            </Row>
          </Col>
          <Col style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-end',
          }}
          >
            <p style={{ margin: 0, fontSize: 16 * multiplier }}>{`${time}`}</p>
          </Col>
        </Row>
      </div>
    </WidgetWrapper>
  );
}

export default HelloWidget;
