import React, { useEffect } from 'react';
import ReactGA from 'react-ga';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import history from './history';
import Header from './components/Header';
import Footer from './components/Footer';
import Home from './pages/Home/index';
import Projects from './pages/Projects/index';
import About from './pages/About/index';
import Contact from './pages/Contact/index';
import NoMatch from './pages/NoMatch';

function initializeReactGA() {
  ReactGA.initialize('UA-131193393-1');
  ReactGA.pageview('/');
}

function App() {
  useEffect(() => {
    initializeReactGA();
  }, []);

  return (
    <Router history={history}>
      <Header />
      <Switch>
        <Route path="/projects/:id?">
          <Projects />
        </Route>
        <Route path="/about/:id?">
          <About />
        </Route>
        <Route path="/contact">
          <Contact />
        </Route>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="*">
          <NoMatch />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
