import React from 'react';
import { Row, Col } from 'antd';

function Container({ justify = 'center', style, children }) {
  return (
    <Row justify={justify}>
      <Col style={styles.main(style)}>
        {children}
      </Col>
    </Row>
  );
}

const styles = {
  main: (style) => ({
    maxWidth: 900,
    margin: 10,
    ...style,
  }),
};

export default Container;
