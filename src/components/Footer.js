import React from 'react';
import { Layout } from 'antd';

const { Footer: AntdFooter } = Layout;

function Footer() {
  return (
    <AntdFooter style={{ textAlign: 'center' }}>
      Elias Lankinen ©
      {new Date().getFullYear()}
    </AntdFooter>
  );
}

export default Footer;
