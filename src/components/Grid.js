import React from 'react';
import { Row, Col } from 'antd';

function Grid({ data, perRow, itemComponent }) {
  const table = [];
  let i = 0;
  while (i < data.length) {
    table.push(
      <Row key={i} justify="left">
        {[...Array(perRow).keys()].map((add, j) => {
          if (i + add < data.length) {
            const item = data[i + add];
            return (
              <Col key={i.toString() + j.toString()} xs={Math.ceil(24 / perRow)}>
                {itemComponent(item)}
              </Col>
            );
          }
          return null;
        })}
      </Row>,
    );
    i += perRow;
  }
  return table;
}

export default Grid;
