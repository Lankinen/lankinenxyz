import React, { useEffect, useState } from 'react';
import { Layout, Row, Col } from 'antd';
import { Link, useLocation } from 'react-router-dom';
import { createGlobalStyle } from 'styled-components';
import msSansSerif from 'react95/dist/fonts/ms_sans_serif.woff2';
import useWindowSize from '../hooks/useWindowSize';

const { Header: AntdHeader } = Layout;

const GlobalStyles = createGlobalStyle`
  @font-face {
    font-family: 'ms_sans_serif';
    src: url('${msSansSerif}') format('woff2');
    font-weight: 400;
    font-style: normal
  }
`;

function Header() {
  const windowSize = useWindowSize();
  const location = useLocation();

  const [showDropdown, setShowDropdown] = useState(true);

  const isSelected = (path) => {
    const beginning = location.pathname.split('/').slice(0, 2).join('/');
    return beginning === path;
  };

  useEffect(() => {
    setShowDropdown(false);
  }, [location]);

  return (
    <AntdHeader style={{
      backgroundColor: 'lightgrey', paddingLeft: 0, paddingRight: 0, position: 'sticky', top: 0, zIndex: 999,
    }}
    >
      <GlobalStyles />
      {showDropdown && (
        <div style={{
          position: 'absolute', top: 64, backgroundColor: 'lightgrey', width: '100%', minHeight: 0, zIndex: 1000,
        }}
        >
          <HeaderItems isSelected={isSelected} vertical />
        </div>
      )}
      <Row>
        <Col xs={21}>
          <Row>
            <Link to="/" style={styles.textContainer}>
              <p style={{ marginTop: -3, ...styles.text('VT323, monospace', 35, '#D81E5B') }}>
                Lankinen.xyz
              </p>
            </Link>
            {windowSize.width > 620 && (
              <HeaderItems isSelected={isSelected} />
            )}
          </Row>
        </Col>
        <Col xs={3}>
          {windowSize.width <= 620 ? (
            <p style={{ cursor: 'pointer', ...styles.text('', 40, '#D81E5B') }} onClick={() => setShowDropdown(!showDropdown)}>=</p>
          ) : (
            <span style={{ fontSize: 30 }} role="img" aria-label="usa">🇺🇸</span>
          )}
        </Col>
      </Row>
    </AntdHeader>
  );
}

const styles = {
  textContainer: {
    height: 64,
  },
  text: (fontFamily, fontSize, color) => ({
    color,
    fontSize,
    fontFamily,
    paddingLeft: 10,
    paddingRight: 10,
  }),
};

function HeaderItems({ isSelected, vertical }) {
  const isSelectedColor = (path) => (isSelected(path) ? '#0c3f61' : 'black');

  return (
    <>
      <Link to="/projects" style={vertical ? {} : styles.textContainer}>
        <p style={styles.text('Roboto, monospace', 17, isSelectedColor('/projects'))}>
          Projects
        </p>
      </Link>
      <Link to="/about" style={vertical ? {} : styles.textContainer}>
        <p style={styles.text('Roboto, monospace', 17, isSelectedColor('/about'))}>
          About
        </p>
      </Link>
      <Link to="/contact" style={vertical ? {} : styles.textContainer}>
        <p style={styles.text('Roboto, monospace', 17, isSelectedColor('/contact'))}>
          Contact
        </p>
      </Link>
      <a href="https://www.notion.so/lankinen/Lankinen-92c1d0531acd48f99c4dcdd546ad1a68" target="_blank" rel="noreferrer">
        <p style={styles.text('Roboto, monospace', 17, 'blue')}>
          Notion
        </p>
      </a>
    </>
  );
}

export default Header;
