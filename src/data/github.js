async function getRepos() {
  return fetch('https://us-central1-apis-292411.cloudfunctions.net/my-social?github')
    .then((response) => response.text())
    .then((text) => text)
    .catch(() => (
      '0'
    ));
}

export default getRepos;
