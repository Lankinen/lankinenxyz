async function getWeather() {
  return fetch('https://us-central1-apis-292411.cloudfunctions.net/weather')
    .then((response) => response.text())
    .then((text) => text)
    .catch(() => (
      '0'
    ));
}

export default getWeather;
