async function randomQuote() {
  return fetch('https://type.fit/api/quotes')
    .then((response) => response.json())
    .then((json) => {
      const selected = json[Math.floor(Math.random() * json.length)];
      return `"${selected.text}" - ${selected.author}`;
    })
    .catch(() => (
      '"Legacy is greater than currency" - Gary Vaynerchuk'
    ));
}

export default randomQuote;
