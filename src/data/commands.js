import emailjs from 'emailjs-com';

const localTime = () => new Date().toLocaleString('en-US', { timeZone: 'Europe/Helsinki' });

const estimateRespond = () => {
  const ctime = new Date(localTime());
  const chours = ctime.getHours();
  let respondTime = 0;
  if (chours > 20 || chours < 8) {
    if (chours >= 0 && chours < 13) {
      respondTime = 8 - chours;
    } else {
      respondTime = 8 + (24 - chours);
    }
  }
  let message = `${respondTime} hours`;
  if (respondTime === 1) {
    message = '1 hour';
  }
  if (respondTime === 0) {
    message = 'less than 1 hour';
  }
  return message;
};

const validateEmail = (email) => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

const mailExample = 'mail steve@apple.com hey elias, how are you doing?';

const master = [
  {
    command: 'mail',
    action: (args, print, runCommand) => {
      if (args.length < 3) {
        print(`Command format is incorrect. e.g. ${mailExample}`);
        return;
      }
      const email = args[1];
      const text = args.slice(2).join(' ');
      if (!validateEmail(email)) {
        print('Email is in wrong format.');
        return;
      }

      print('sending');

      const templateParams = {};
      templateParams.reply_to = email;
      templateParams.message = text;

      emailjs.send('service_2p97hnu', 'template_db4g3wi', templateParams, 'user_qnTGRKHSWrzbY1jonS2Jh')
        .then((result) => {
          print('Thank you! I will reply as soon as possible.');
          runCommand('respond-time');
          console.log('result.text:', result.text);
        }, (error) => {
          print(`Error: ${error}`);
          print('Please send the email directly to elias@lankinen.xyz and let me know about the error.');
          console.log('error.text:', error.text);
        });
    },
    help: `[your email, message] - Send message to my email elias@lankinen.xyz. e.g. ${mailExample}`,
  },
  {
    command: 'respond-time',
    action: (args, print, runCommand) => {
      print(`Estimate respond time right now is ${estimateRespond()}`);
    },
    help: 'Get estimate how long it takes for me to respond messages sent right now.',
  },
  {
    command: 'local-time',
    action: (args, print, runCommand) => {
      print(localTime());
      print('This is hard coded which means that it might not be up to date.');
    },
    help: 'Get my local time',
  },
  {
    command: 'twitter',
    action: (args, print, runCommand) => {
      window.open('https://twitter.com/reallankinen', '_blank');
    },
    help: "Open my Twitter. It's often better to send me an email but if you are in rush, you can try tweeting or sending private message.",
  },
  {
    command: 'hello',
    action: (args, print, runCommand) => {
      print('hi');
    },
    help: 'Say hi.',
  },
];

const commands = {};
const descriptions = {};
master.forEach((item) => {
  commands[item.command] = item.action;
  descriptions[item.command] = item.help;
});

export { commands, descriptions };
