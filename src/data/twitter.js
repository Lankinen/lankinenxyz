async function getTwitterFollowers() {
  return fetch('https://us-central1-apis-292411.cloudfunctions.net/my-social?twitter=followers')
    .then((response) => response.text())
    .catch(() => (
      '0'
    ));
}

async function getPinned() {
  return fetch('https://us-central1-apis-292411.cloudfunctions.net/my-social?twitter=pinned')
    .then((response) => response.text())
    .catch(() => (
      '1316631648960688130'
    ));
}

export { getTwitterFollowers, getPinned };
