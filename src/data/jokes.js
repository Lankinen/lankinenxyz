async function randomJoke() {
  return fetch('https://official-joke-api.appspot.com/jokes/programming/random')
    .then((response) => response.json())
    .then((json) => [json[0].setup, json[0].punchline])
    .catch(() => (
      (['There was an error when getting a joke.', 'This is not a joke.'])
    ));
}

export default randomJoke;
