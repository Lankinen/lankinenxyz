[
  {
    "name": "Grapho",
    "year": "2020",
    "icon": "grapho.png",
    "urls": [
      "https://grapho.xyz"
    ],
    "images": [
      "/images/grapho_elon.png"
    ],
    "idea": "Whatsapp style online messenger. Speech-to-text transcribed voice messages and text-to-speech created voice for text messages.",
    "tech": "React Native, Firebase, Python, Swift",
    "quit": "The improvement was not big enough to get users to ask their contacts to move to a new communication app. I made an iMessage plugin but it did not take off either.",
    "learnings": "I under estimated the amount it requires from product to succeed in established markets. It is hard to create 10x better products when thousands of people have tried the same for years and there are one or more big players."
  },
  {
    "name": "deepez.ai",
    "year": "2019",
    "icon": "deepez.png",
    "urls": [
      "https://deepez.ai"
    ],
    "images": [
      "/images/deepez_home_page.png"
    ],
    "idea": "Machine learning as a service for businesses. The catchline was \"Start using machine learning in your product with two lines of code.\"",
    "tech": "Python, PyTorch, ReactJS, TypeScript, Python, Django, SQL, PostgreSQL",
    "quit": "An average software engineer wasn't available to understand where ML can be used and data scientists didn't see the model creation as a big part of their job. People who understand the basics but are looking to prototype quickly could have seen this as a useful tool but they often needed more custom models.",
    "learnings": "Importance of doing customer interviews and understanding them before getting started. Offering free services is an easy way to get customers but the approach backfires as the customers become passive when they are not invested. In hindsight, it would have been a better approach to do something for a specific use case like a recommendation engine for grocery stores."
  },
  {
    "name": "Trimmed News",
    "year": "2018",
    "icon": "trimmednews.png",
    "images": [
      "/images/trimmednews_summary.png",
      "/images/trimmednews_interest.png"
    ],
    "idea": "A news aggregator app for reading summarized news.",
    "tech": "React Native, JavaScript, Firebase Firestore, Python, PyTorch, Docker",
    "quit": "The useful feature was summarization but I implemented it into my own news app that wasn't otherwise in the level of the alternatives like Google News. I found even myself rather using Google News without summaries as the news were more relevant.",
    "learnings": "Instead of creating the whole app, I should have started by creating a newsletter for niche markets and do the summarization manually. There I could have seen if they care about the summarized news enough to make sense build the app instead of spending months building the app to realize that it's not important enough to matter alone."
  },
  {
    "name": "Retro Racing Online",
    "year": "2017",
    "icon": "retro_racing_online.png",
    "images": [
      "/images/retroracingonline_track.jpg"
    ],
    "idea": "A top-down mobile racing game where users could play against each other.",
    "tech": "Unity, C#, PHP, SQL, MySQL",
    "quit": "The small absolute number of downloads discouraged me to invest more time. This wasn't a good reason to quit and later I regretted for not continuing to see the full potential.",
    "learnings": "Growth is more important than absolute numbers and the number of downloads is not a good metric. This is the first app where I got feedback by sending cold messages to people on Slack. I learned the importance of app store optimization and how to do it effectively."
  },
  {
    "name": "Free Roll Poker",
    "year": "2014",
    "icon": "free_roll_poker.png",
    "images": [
      "/images/freerollpoker_table.png"
    ],
    "idea": "Texas Holdem poker where thousands of users could play in free tournaments and win real money. The money came from the ads displayed during the gameplay.",
    "tech": "Unity, C#, PHP, SQL, MySQL",
    "quit": "Google Play Store's bots rejected this claiming it to violate their rules. I did not agree with the conclusion but I could not get a human to talk. The game had some bugs I was not available to find because the code was written into a few files making it messy.",
    "learnings": "This was my first bigger project and I learned the importance of clean code after writing nearly everything into a few different files. The biggest file was close to 10,000 lines long."
  }
]